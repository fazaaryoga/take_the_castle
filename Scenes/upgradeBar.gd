extends HBoxContainer


signal gauge_full


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _process(_delta):
	if $Gauge.value >= 100:
		emit_signal("gauge_full")
