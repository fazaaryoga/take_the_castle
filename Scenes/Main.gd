extends Node2D


var offset = 0.0
onready var soldier = preload("res://Scenes/Soldier.tscn")
export(String, "red", "blue", "yellow") var playerTeam = "red"
onready var castleNum = get_tree().get_nodes_in_group("castles").size()

# Called when the node enters the scene tree for the first time.
func _ready():
	$Label.text = "You are the " + playerTeam + " team"


func _on_Castle_send_soldier():
	print("signal received")
	var path = PathFollow2D.new()
	$Path2D.add_child(path)
	print("path instanced")
	path.add_child(soldier.instance())
	print("soldier instanced")
	
func _on_Castle_Taken(team):
	var team_count = get_tree().get_nodes_in_group(team).size()
	if team_count == castleNum:
		if team == playerTeam:
			$"Win Modal".enable()
			pass
		else:
			$"Lose Modal".enable()
			pass	
	
func _on_Timer_timeout():
	$Label.hide()
