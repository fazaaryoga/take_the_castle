extends Area2D


var moveSpeed = 200;
var team: String
var path: = PoolVector2Array()
var target: String
var direction = Vector2.DOWN

# Called when the node enters the scene tree for the first time.
func _ready():
	add_to_group("soldiers")


func _process(delta):
	var walkDist = moveSpeed * delta
	while walkDist > 0 and path.size() > 0:
		var nextPointDist = global_position.distance_to(path[0])
		if walkDist < nextPointDist:
			direction = global_position.direction_to(path[0])
			position += direction * walkDist
			$AnimatedSprite.animation = team + "_walk_" + getAnimDirection(direction)
			
		else:
			global_position = path[0]
			path.remove(0)
		walkDist -= nextPointDist
		

func init(init_team, init_path, init_target):
	team = init_team
	path = init_path
	target = init_target
	$AnimatedSprite.animation = team+"_walk_" + getAnimDirection(direction)
	set_process(true)

func getAnimDirection(direction: Vector2):
	var norm_direction = direction.normalized()
	if norm_direction.y >= 0.707:
		return "front"
	elif norm_direction.y <= -0.707:
		return "back"
	elif norm_direction.x <= -0.707:
		$AnimatedSprite.flip_h = true
		return "side"
	elif norm_direction.x >= 0.707:
		$AnimatedSprite.flip_h = false
		return "side"


func _on_Soldier_area_entered(area):
	if area.name == target:
		$CollisionShape2D.set_deferred("disabled", true)
		hide()
		queue_free()
