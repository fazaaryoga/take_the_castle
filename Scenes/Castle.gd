extends Area2D

signal castleTaken

onready var hpBarGauge = $HP_bar/Gauge
onready var hpBarLabel = $HP_bar/MarginContainer/NinePatchRect/CapacityLabel
onready var upgradeBarGauge = $Upgrade_Bar/Gauge
onready var soldier = preload("res://Scenes/Soldier.tscn")

export(int) var level = 1
export(String, "red", "blue", "yellow") var team = "red"
var capacity = 0
var maxCapacity = 20 * level
var upgradeProgress = 0
var active = false
var currentCastle
var path
var deployed = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	connect("castleTaken", get_parent(), "_on_Castle_Taken")
	hpBarGauge.max_value = maxCapacity
	updateHpBar()
	updateUpgradeBar()
	currentCastle = team + "_" + str(level)
	$AnimatedSprite.animation = currentCastle
	$AnimatedSprite.play()

#func _process(delta):
#	pass

func get_input():
	var click = Input.is_action_just_pressed("ui_click")
	
	if click:
		print("castle clicked")
		emit_signal("send_soldier")

func updateHpBar():
	hpBarGauge.value = capacity
	hpBarLabel.text = str(capacity)
	
func updateUpgradeBar():
	upgradeBarGauge.value = upgradeProgress

func _on_CapacityFillTimer_timeout():
	if capacity < maxCapacity:
		fillCapacity(1)
	elif capacity == maxCapacity:
		if level < 4:
			upgradeProgress += 2
		updateUpgradeBar()
	elif capacity > maxCapacity:
		capacity -= 2
		updateHpBar()
		if level < 4:
			upgradeProgress += 10
			updateUpgradeBar()
	
	if team != get_parent().playerTeam:
		var do_attack = bot_decide()
		if do_attack:
			bot_select()
		

func upgradeCastle():
	$castle_upgrade.play()
	upgradeProgress = 0
	updateUpgradeBar()	
	updateHpBar()
	if level < 4:
		level += 1
		maxCapacity = 20 * level
		hpBarGauge.max_value = maxCapacity
		currentCastle = team + "_" + str(level)
		$AnimatedSprite.animation = currentCastle
		$AnimatedSprite.play()
		
func attack(pos, target):
	if target == name:
		return
	
	if active and team == get_parent().playerTeam:
		print("deploying")
		active = false
		$ArrowsRight2.visible = !$ArrowsRight2.visible
		path = $"../Navigation2D".get_simple_path(position, pos)
		deployed = capacity / 2
		capacity /= 2
		deploySoldiers(path, deployed, target)
	
func deploySoldiers(attack_path, amt, target):
	for _i in range(amt):
		yield(get_tree().create_timer(0.1), "timeout")
		var unit = soldier.instance()
		unit.init(team, attack_path, target)
		add_child(unit)
	

func _on_Upgrade_Bar_gauge_full():
	upgradeCastle()
	$Upgrade_Bar/Gauge.value = 0

func _on_Castle_area_entered(area):
	if area.target == name:
		if area.team != team:
			capacity -= 1
			updateHpBar()
			if capacity < 0:
				reset(area.team)
		else:
			fillCapacity(1)

func reset(newTeam):
	$castle_taken.play()
	level = 1
	capacity = 0
	remove_from_group(team)
	team = newTeam
	add_to_group(team)
	maxCapacity = 20 * level
	upgradeProgress = 0
	hpBarGauge.max_value = maxCapacity
	currentCastle = team + "_" + str(level)
	$AnimatedSprite.animation = currentCastle
	$AnimatedSprite.play()
	updateHpBar()
	updateUpgradeBar()
	emit_signal("castleTaken", team)
	
func fillCapacity(amt):
	capacity += amt
	updateHpBar()


func drainUpgrade(amt):
	capacity -= amt
	if level < 4:
		upgradeProgress += 5
		updateUpgradeBar()


func _on_Button_gui_input(event):
	if event is InputEventMouseButton and event.pressed:
		match event.button_index:
			BUTTON_LEFT:
				print("left click")
				if team == get_parent().playerTeam:
					active = !active
					$ArrowsRight2.visible = !$ArrowsRight2.visible
				else:
					get_tree().call_group("castles", "attack", position, name)
			BUTTON_RIGHT:
				print("right click")
				if team == get_parent().playerTeam:
					active = false
					$ArrowsRight2.visible = false
					get_tree().call_group(team, "attack", position, name)


func bot_decide():
	var chance = randi() % 100 + 1
	var capPercentage = (float(capacity) / float(maxCapacity)) * 100
	if chance < capPercentage:
		return true
	else:
		return false
		
func bot_attack(targetPos, target):
	path = $"../Navigation2D".get_simple_path(position, targetPos)
	deployed = capacity / 2
	capacity /= 2
	deploySoldiers(path, deployed, target)
	
func bot_select():
	var castles = get_tree().get_nodes_in_group("castles")
	castles.remove(castles.find(self))
	var index = randi() % castles.size()
	var selected = castles[index]
	print(name + " attacks " + selected.name)
	bot_attack(selected.position, selected.name)
