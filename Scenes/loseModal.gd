extends ColorRect

export var disabled = true

# Called when the node enters the scene tree for the first time.
func _ready():
	$"Return to Main Menu".disabled = true
	$Quit.disabled = true
	hide()


func enable():
	$"Return to Main Menu".disabled = false
	$Quit.disabled = false
	show()


func _on_LinkButton_pressed():
	get_tree().change_scene("res://Scenes/MainMenu.tscn")


func _on_LinkButton2_pressed():
	get_tree().quit()
